package org.cleaninsights.example;

import android.util.Log;

import org.cleaninsights.sdk.DoneHandler;
import org.cleaninsights.sdk.JavaStore;
import org.cleaninsights.sdk.Store;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class MyStore extends JavaStore {

    MyStore() {
        super(new HashMap<>(), message -> Log.d("", message));
    }


    @Override
    public void persist(boolean async, @NotNull DoneHandler done) {

    }

    @Override
    public void send(@NotNull String data, @NotNull URL server, double timeout, @NotNull DoneHandler done) {

    }

    @Nullable
    @Override
    public Store load(@NotNull Map<String, ?> args) {
        return null;
    }
}
